# IDS 721 Mini Project 3: S3 Bucket Creation Using AWS CDK & CodeWhisperer

## Overview

Welcome to the repository for Mini-Project 3 in IDS 721. This project demonstrates the process of creating an Amazon S3 bucket utilizing the AWS Cloud Development Kit (CDK) and the innovative tool AWS CodeWhisperer.

## Objectives

- **S3 Bucket Creation**: Leverage AWS CDK to programmatically create an S3 bucket.
- **Code Generation**: Utilize AWS CodeWhisperer for generating efficient CDK code snippets.
- **Enhanced Bucket Features**: Implement additional S3 bucket features, including versioning and encryption, to enhance data security and management.

## Deliverables

### CDK App Code

The code for the CDK application is located in the `lib/` and `bin/` directories of this repository. It includes:

- `lib/miniproject3-stack.ts`: Contains the main stack configuration for the S3 bucket.
- `bin/miniproject3.ts`: Entry point of the CDK application with necessary environment setups.

### Generated S3 Bucket Screenshot

Screenshots of the successfully deployed S3 bucket, showcasing enabled features like versioning and encryption:

![S3 Bucket Snapshot 1](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG22.jpg)
![S3 Bucket Snapshot 2](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG23.jpg)
![S3 Bucket Snapshot 3](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG24.jpg)

### Writeup Explaining CodeWhisperer Usage

AWS CodeWhisperer has been instrumental in generating code snippets for the S3 bucket creation. The process involved:

- Providing prompts like "// make an S3 bucket with versioning and encryption" in the CDK code files.
- CodeWhisperer then suggested appropriate code snippets which were reviewed and integrated into the project.
- This approach significantly streamlined the development process, demonstrating the potential of AI-assisted coding.


## Detailed Implementation Steps

### Environment Setup and Permissions

1. **AWS Account Preparation**:
    - Register for an AWS account and sign up for Codecatalyst [here](https://aws.amazon.com/codecatalyst/).

2. **Development Environment**:
    - Create a new development environment in AWS Cloud9.
    - Select "Create an empty Dev Environment" and initiate the environment setup.

3. **IAM User and Permissions**:
    - Within AWS, create a new IAM user with the following permissions for comprehensive access to required services:
      - `AmazonS3FullAccess`
      - `AWSLambda_FullAccess`
      - `IAMFullAccess`
    - Add two inline policies for extended permissions on `CloudFormation` and `Systems Manager`.

4. **Secure Credential Management**:
    - In the IAM dashboard, navigate to the "Security Credentials" tab of the newly created user.
    - Generate and securely store the `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and choose an appropriate `AWS_REGION` (e.g., `us-west-2`).

### AWS CDK Project Setup

1. **Configure AWS CLI in Cloud9**:
    - Run `aws configure` in the Cloud9 terminal.
    - Enter the access key, secret access key, and choose the default region name (optional) and output format.

2. **CDK Project Initialization**:
    - Create and navigate to a new project directory.
    - Initialize a new CDK TypeScript project using `cdk init app --language typescript`.

3. **Utilizing AWS CodeWhisperer**:
    - With the basic CDK template ready, employ AWS CodeWhisperer to generate custom code for the S3 bucket.
    - Implement the generated code in the `lib/` and `bin/` directories.

4. **Building and Deploying**:
    - Execute `npm run build` to build the CDK project.
    - Generate the CloudFormation template using `cdk synth`.
    - Deploy the stack with `cdk deploy` and verify the S3 bucket creation in the AWS console.

### AWS CodeWhisperer Usage

- AWS CodeWhisperer is utilized to expedite the code generation process. The following prompts were used:
  - In `lib/mini_proj3-stack.ts`, prompts like "// make an S3 bucket" were used to generate the S3 bucket code with versioning and encryption.
  - In `bin/mini_proj3.ts`, prompts such as "// add necessary variables for S3 bucket creation" assisted in setting up the necessary variables.

## Additional Insights and References

### Project Purpose

- This project serves as an introductory exercise in Infrastructure as Code (IaC), focusing on deploying AWS services effectively without manual intervention via the AWS console.

### Troubleshooting and Further Steps

- In case of deployment issues, follow the error messages to identify and resolve role-related problems. This might involve creating and attaching roles with specific permissions.
- For a comprehensive understanding and additional guidance, refer to the following resources:
  - [AWS CDK Documentation](https://docs.aws.amazon.com/cdk/latest/guide/home.html)
  - [CDK TypeScript S3 Bucket Guide](https://docs.aws.amazon.com/cdk/api/latest/docs/aws-s3-readme.html)

### Feedback on AWS CodeWhisperer

- AWS CodeWhisperer, while helpful in starting code generation, may require patience due to its response time. It is best used as a starting point for code development rather than for complete code creation.
