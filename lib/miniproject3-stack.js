"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Miniproject3Stack = void 0;
const cdk = require("aws-cdk-lib");
const iam = require("aws-cdk-lib/aws-iam");
// import * as sqs from 'aws-cdk-lib/aws-sqs';
class Miniproject3Stack extends cdk.Stack {
    constructor(scope, id, props) {
        super(scope, id, props);
        // make an s3 bucket and add bucket properties like versioning and encryption
        const bucket = new cdk.aws_s3.Bucket(this, 'Zaolin-mini3', {
            versioned: true,
            encryption: cdk.aws_s3.BucketEncryption.S3_MANAGED,
            removalPolicy: cdk.RemovalPolicy.DESTROY,
        });
        bucket.grantRead(new iam.AccountRootPrincipal());
    }
}
exports.Miniproject3Stack = Miniproject3Stack;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWluaXByb2plY3QzLXN0YWNrLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibWluaXByb2plY3QzLXN0YWNrLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBLG1DQUFtQztBQUVuQywyQ0FBMkM7QUFDM0MsOENBQThDO0FBRTlDLE1BQWEsaUJBQWtCLFNBQVEsR0FBRyxDQUFDLEtBQUs7SUFDOUMsWUFBWSxLQUFnQixFQUFFLEVBQVUsRUFBRSxLQUFzQjtRQUM5RCxLQUFLLENBQUMsS0FBSyxFQUFFLEVBQUUsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUV4Qiw2RUFBNkU7UUFDN0UsTUFBTSxNQUFNLEdBQUcsSUFBSSxHQUFHLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsY0FBYyxFQUFFO1lBQ3pELFNBQVMsRUFBRSxJQUFJO1lBQ2YsVUFBVSxFQUFFLEdBQUcsQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsVUFBVTtZQUNsRCxhQUFhLEVBQUUsR0FBRyxDQUFDLGFBQWEsQ0FBQyxPQUFPO1NBQ3pDLENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLENBQUMsb0JBQW9CLEVBQUUsQ0FBQyxDQUFDO0lBRW5ELENBQUM7Q0FFRjtBQWZELDhDQWVDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgY2RrIGZyb20gJ2F3cy1jZGstbGliJztcbmltcG9ydCB7IENvbnN0cnVjdCB9IGZyb20gJ2NvbnN0cnVjdHMnO1xuaW1wb3J0ICogYXMgaWFtIGZyb20gJ2F3cy1jZGstbGliL2F3cy1pYW0nO1xuLy8gaW1wb3J0ICogYXMgc3FzIGZyb20gJ2F3cy1jZGstbGliL2F3cy1zcXMnO1xuXG5leHBvcnQgY2xhc3MgTWluaXByb2plY3QzU3RhY2sgZXh0ZW5kcyBjZGsuU3RhY2sge1xuICBjb25zdHJ1Y3RvcihzY29wZTogQ29uc3RydWN0LCBpZDogc3RyaW5nLCBwcm9wcz86IGNkay5TdGFja1Byb3BzKSB7XG4gICAgc3VwZXIoc2NvcGUsIGlkLCBwcm9wcyk7XG5cbiAgICAvLyBtYWtlIGFuIHMzIGJ1Y2tldCBhbmQgYWRkIGJ1Y2tldCBwcm9wZXJ0aWVzIGxpa2UgdmVyc2lvbmluZyBhbmQgZW5jcnlwdGlvblxuICAgIGNvbnN0IGJ1Y2tldCA9IG5ldyBjZGsuYXdzX3MzLkJ1Y2tldCh0aGlzLCAnWmFvbGluLW1pbmkzJywge1xuICAgICAgdmVyc2lvbmVkOiB0cnVlLFxuICAgICAgZW5jcnlwdGlvbjogY2RrLmF3c19zMy5CdWNrZXRFbmNyeXB0aW9uLlMzX01BTkFHRUQsXG4gICAgICByZW1vdmFsUG9saWN5OiBjZGsuUmVtb3ZhbFBvbGljeS5ERVNUUk9ZLFxuICAgIH0pO1xuICAgIFxuICAgIGJ1Y2tldC5ncmFudFJlYWQobmV3IGlhbS5BY2NvdW50Um9vdFByaW5jaXBhbCgpKTtcblxuICB9XG5cbn1cbiJdfQ==